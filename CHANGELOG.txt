#########
Changelog
#########

0.3.1 (2018-08-22)
==================

* created detail view so that you can link to one specific message
* added search to django admin for to email addresses


0.2 (2015-09-08)
================

* Added two mixins that use the CombinedEmailBackend to use multiple email backends.
* Optimized the generated filename to prevent the creation of multiple files with the same name during a bulk submission.


0.1 (2015-03-24)
================

* Initial release
