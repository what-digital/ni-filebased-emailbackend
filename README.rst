NI Filebased Emailbackend documentation
#######################################

|
Info
****
This app provides an email backend that sends the email to a file and creates a database record. You can browse and
download the messages in the admin section of Django.

|
Installation
************

The ni-filebased-emailbackend depends on:

* django-configurations>=0.3
* django-multiple-email-backends>=0.1a4
|
Install using ``pip``::

    pip install http://www.notch-interactive.com/media/pyni/ni_filebased_emailbackend-0.2.tar.gz

| 
Add ``'ni_filebased_emailbackend'`` to your INSTALLED_APPS setting::

    INSTALLED_APPS = (
        ...
        'ni_filebased_emailbackend',
    )

|
Since we added a new app, we need to update our database::

    python manage.py migrate ni_filebased_emailbackend
|
Let your settings class inherit from the ``FilebasedEmailBackendMixin``::

    from ni_filebased_emailbackend.settings import FilebasedEmailBackendMixin


    class StageDefaultSite(FilebasedEmailBackendMixin, BaseSettings):
        ...
|
If you want to use multiple email backends, you can use the ``CombinedSMTPAndFilebasedEmailBackendMixin`` or the
``CombinedConsoleAndFilebasedEmailBackendMixin``::

    from ni_filebased_emailbackend.settings import CombinedSMTPAndFilebasedEmailBackendMixin


    class LiveDefaultSite(CombinedSMTPAndFilebasedEmailBackendMixin, BaseSettings):
        ...
