# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import FileEmailLog


class FileEmailLogAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'get_submission_date',
        'get_subject',
        'get_from_email',
        'get_to',
        'get_download_link'
    )

    fields = (
        'id',
        'get_submission_date',
        'get_subject',
        'get_from_email',
        'get_to',
        'get_download_link',
    )

    readonly_fields = fields

    search_fields = (
        "to_address",
        "subject",
    )

    list_filter = (
        'created',
        'submission_date',
    )

    mail_object_store = {}

    def get_download_link(self, obj):
        return u'<a href="{media_url}emails/{filename}" target="_blank">' \
               u'{download}</a>'.format(media_url=settings.MEDIA_URL,
                                        filename=obj.filename,
                                        download=_(u"download"))

    get_download_link.allow_tags = True
    get_download_link.short_description = _(u"Download")

    def get_submission_date(self, obj):
        try:
            if obj.id not in self.mail_object_store.keys():
                self.mail_object_store[obj.id] = obj.get_mail_object()
            return self.mail_object_store[obj.id].submission_date
        except:
            return ""

    get_submission_date.short_description = _(u"Submission date")

    def get_subject(self, obj):
        try:
            if obj.id not in self.mail_object_store.keys():
                self.mail_object_store[obj.id] = obj.get_mail_object()
            return self.mail_object_store[obj.id].subject
        except:
            return "no subject"

    get_subject.short_description = _(u"Subject")

    def get_from_email(self, obj):
        try:
            if obj.id not in self.mail_object_store.keys():
                self.mail_object_store[obj.id] = obj.get_mail_object()
            return self.mail_object_store[obj.id].from_email
        except:
            return "no email"

    get_from_email.short_description = _(u"Sender")

    def get_to(self, obj):
        try:
            if obj.id not in self.mail_object_store.keys():
                self.mail_object_store[obj.id] = obj.get_mail_object()
            return self.mail_object_store[obj.id].to
        except:
            return "no email"

    get_to.short_description = _(u"Recipients")


admin.site.register(FileEmailLog, FileEmailLogAdmin)
