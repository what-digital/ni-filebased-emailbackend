# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class FilebasedEmailbackendApp(AppConfig):
    name = 'ni_filebased_emailbackend'
    verbose_name = _("E-Mail Log")
    label = "ni_filebased_emailbackend"
