from django.core.mail.backends import filebased
from django.utils.timezone import now
import os
import uuid

from ...models import FileEmailLog


class EmailBackend(filebased.EmailBackend):
    """
    Save an email as `.eml` file using the current datetime as filename. All mails are stored as FileMailLog and can
    be downloaded in the admin.
    """
    filename = None

    def _get_filename(self):
        from django.conf import settings
        if self._fname is None:
            self.filename = '{timestamp}_{id}.eml'.format(
                timestamp=now().strftime("%Y%m%d%H%M%S"),
                id=uuid.uuid4()
            )
            self._fname = os.path.join(settings.PROJECT_ROOT, self.file_path, self.filename)
        return self._fname

    def send_messages(self, email_messages):
        num_messages_sent = super(EmailBackend, self).send_messages(email_messages)

        # We create a log entry with the filename for the admin section of this app.
        FileEmailLog.objects.create(filename=self.filename)

        return num_messages_sent
