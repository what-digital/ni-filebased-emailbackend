# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ni_filebased_emailbackend', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fileemaillog',
            options={'verbose_name': 'E-mail', 'verbose_name_plural': 'E-mails'},
        ),
    ]
