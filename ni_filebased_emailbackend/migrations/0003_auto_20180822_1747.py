# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ni_filebased_emailbackend', '0002_auto_20170505_1035'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileemaillog',
            name='subject',
            field=models.CharField(max_length=2048, null=True),
        ),
        migrations.AddField(
            model_name='fileemaillog',
            name='submission_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='fileemaillog',
            name='to_address',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
