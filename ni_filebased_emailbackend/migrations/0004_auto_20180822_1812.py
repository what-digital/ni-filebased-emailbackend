# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from ..models import FileEmailLog


def add_method_fields_to_db(apps, schema_editor):
    for mail_log in FileEmailLog.objects.all():
        try:
            mail_log.save()
        except IOError:
            # an eml file is missing we cant really save this instance. Dooh.
            pass


class Migration(migrations.Migration):

    dependencies = [
        ('ni_filebased_emailbackend', '0003_auto_20180822_1747'),
    ]

    operations = [
        migrations.RunPython(add_method_fields_to_db),
    ]
