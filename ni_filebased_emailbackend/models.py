# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .outbox import Outbox
from django.conf import settings


class FileEmailLog(models.Model):
    created = models.DateTimeField(auto_now=True)
    filename = models.CharField(max_length=255)

    to_address = models.CharField(max_length=255, null=True)
    subject = models.CharField(max_length=2048, null=True)
    submission_date = models.DateField(null=True)

    class Meta:
        verbose_name = _(u"E-mail")
        verbose_name_plural = _(u"E-mails")

    def __unicode__(self):
        return self.filename or _(u"untitled")

    def get_mail_object(self):
        return Outbox().get(self.filename)

    def get_download_link(self):
        return u'<a href="{media_url}emails/{filename}" target="_blank">' u'{download}</a>'.format(
            media_url=settings.MEDIA_URL,
            filename=self.filename,
            download=_(u"download")
        )

    get_download_link.allow_tags = True

    def get_submission_date(self):
        try:
            return self.get_mail_object().submission_date
        except:
            return ""

    submission_date.short_description = _(u"Submission date")

    def get_subject(self):
        try:
            return self.get_mail_object().subject
        except:
            return "no subject"

    def get_from_email(self):
        try:
            return self.get_mail_object().from_email
        except:
            return "no email"

    def get_to(self):
        try:
            return self.get_mail_object().to
        except:
            return "no email"

    def save(self, *args, **kwargs):
        if self.get_mail_object():
            if not self.to_address:
                self.to_address = self.get_mail_object().to
            if not self.to_address:
                self.subject = self.get_mail_object().subject
            if not self.submission_date:
                self.submission_date = self.get_mail_object().submission_date

        super(FileEmailLog, self).save(*args, **kwargs)
