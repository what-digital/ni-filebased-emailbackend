# -*- coding: utf-8 -*-
import datetime
import re

from email.header import decode_header, email
from email.utils import parsedate_tz
from os import path, listdir
from email.parser import Parser


from django.conf import settings


class Outbox(object):
    """
    The outbox reads mails from the filesystem and returns Mail objects.
    The code was copied from the django-outbox app and enhanced to
    support attachments.
    """

    def __init__(self):
        self._parser = Parser()

    def all(self):
        try:
            return list(
                reversed([
                    self._message_from_file(filepath)
                    for filepath in listdir(self.maildirectory)
                ])
            )
        except OSError:
            return []

    def get(self, id):
        return self._message_from_file(id)

    def _message_from_file(self, filename):
        abspath = path.join(self.maildirectory, filename)
        with open(abspath) as f:
            message = self._parser.parse(f)
            return self._convert_message(filename, message, abspath)

    def _convert_message(self, filename, message, abspath):
        if message.is_multipart():
            body = {}
            attachments = []
            # The message might has a nested payload structure.
            for submessage in message.get_payload():
                if type(submessage.get_payload()) == list:
                    for subsubmessage in submessage.get_payload():
                        body, attachments = self._update_body_and_attachments(
                            body=body,
                            attachments=attachments,
                            submessage=subsubmessage
                        )
                else:
                    body, attachments = self._update_body_and_attachments(
                        body=body,
                        attachments=attachments,
                        submessage=submessage
                    )

        else:
            body = {message.get_content_type():
                    self._clear_content(message.get_payload())}

        return Mail(
            id=filename,
            subject=decode_header(message.get('Subject'))[0][0],
            from_email=message.get('From'),
            to=message.get('To'),
            submission_date=datetime.datetime.fromtimestamp(
                email.utils.mktime_tz(parsedate_tz(message.get('Date')))
            ),
            content_type=message.get_content_type(),
            body=body,
            path=abspath
        )

    def _update_body_and_attachments(self, body, attachments, submessage):
        if submessage.get_content_type() in ['mail/html', 'text/plain']:
            body[submessage.get_content_type()] = self._clear_content(
                content=submessage.get_payload()
            )
        else:
            attachments.append(
                {submessage.get_content_type(): submessage.get_payload()}
            )
        return body, attachments

    def _clear_content(self, content):
        return re.sub(r'\n-+', '', content)

    @property
    def maildirectory(self):
        return settings.EMAIL_FILE_PATH


class Mail(object):

    def __init__(self, id, subject, from_email, to, submission_date,
                 content_type, body, path):
        self._id = id
        self._subject = subject
        self._from_email = from_email
        self._to = to
        self._submission_date = submission_date
        self._content_type = content_type
        self._body = body
        self._path = path

    @property
    def id(self):
        return self._id

    @property
    def subject(self):
        return self._subject

    @property
    def from_email(self):
        return self._from_email

    @property
    def to(self):
        return self._to

    @property
    def submission_date(self):
        return self._submission_date

    @property
    def content_type(self):
        return self._content_type

    @property
    def body(self):
        return self._body

    @property
    def path(self):
        return self._path
