from os.path import normpath, join


class FilebasedEmailBackendMixin(object):
    @classmethod
    def pre_setup(cls):
        super(FilebasedEmailBackendMixin, cls).pre_setup()
        cls.EMAIL_BACKEND = 'ni_filebased_emailbackend.mail.backends.filebased.EmailBackend'
        cls.EMAIL_FILE_PATH = normpath(join(cls.MEDIA_ROOT, 'emails'))


class CombinedSMTPAndFilebasedEmailBackendMixin(FilebasedEmailBackendMixin):
    @classmethod
    def pre_setup(cls):
        super(CombinedSMTPAndFilebasedEmailBackendMixin, cls).pre_setup()
        cls.EMAIL_BACKEND = 'django_multiple_email_backends.backend.CombinedEmailBackend'
        cls.EMAIL_BACKEND_LIST = [
            'django.core.mail.backends.smtp.EmailBackend',
            'ni_filebased_emailbackend.mail.backends.filebased.EmailBackend'
        ]


class CombinedConsoleAndFilebasedEmailBackendMixin(FilebasedEmailBackendMixin):
    @classmethod
    def pre_setup(cls):
        super(CombinedConsoleAndFilebasedEmailBackendMixin, cls).pre_setup()
        cls.EMAIL_BACKEND = 'django_multiple_email_backends.backend.CombinedEmailBackend'
        cls.EMAIL_BACKEND_LIST = [
            'django.core.mail.backends.console.EmailBackend',
            'ni_filebased_emailbackend.mail.backends.filebased.EmailBackend'
        ]
