# -*- coding: utf-8 -*-
import codecs
import re
from os import path
from setuptools import find_packages

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def read_file_content(*parts):
    file_path = path.join(path.dirname(__file__), *parts)
    return codecs.open(file_path, encoding='utf-8').read()


def find_version(*parts):
    version_file = read_file_content(*parts)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


setup(
    name='ni_filebased_emailbackend',
    version=find_version('ni_filebased_emailbackend', '__init__.py'),
    description='',
    long_description=read_file_content('README.rst'),
    author='Notch Interactive GmbH',
    author_email='webmaster@notch-interactive.com',
    url='',
    packages=find_packages(),
    include_package_data=True,
    license='',
    install_requires=[
        read_file_content('requirements.txt')
    ],
    keywords='',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Utilities',
    ],
)
